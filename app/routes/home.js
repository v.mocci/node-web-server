var dbconn = require('./../../config/conn');

module.exports = function(app) {

    var conn = dbconn();

    app.get('/', function(req, res) {
        conn.query('select * from gaming_news', function(err, results) {
            res.render("home/index", { news: results });
        });

    });
}