var dbconn = require('./../../config/conn');

module.exports = function(app) {
    var conn = dbconn();

    app.get('/news', function(req, res) {
        conn.query('select * from gaming_news', function(err, results) {
            if (err) {
                console.log("Error while loading page, please contact administrator and send this:", err);
            } else {
                res.render("news/news", { news: results });
            }
        });
    });
}