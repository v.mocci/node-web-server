## About

This project was initiated for testing purposes and common things about routes on Node, GET requisitions and more.

## Contribuitors

- Vinicius Mocci;
- Google;
- God.

## Technologies

- View Engine: EJS
- Database: MySQL
- Framework: Express
- Structure: Common JS
- Bootstrap