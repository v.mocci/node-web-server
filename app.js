var app = require('./config/server');

var rotaHome = require('./app/routes/home')(app);

var rotaNews = require('./app/routes/news')(app);

app.listen(8081, function() {
    console.log("The server is on! Please use on port 8081 of your local machine.")
});