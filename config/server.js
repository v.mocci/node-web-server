//This project uses Express
var express = require('express');
var app = express();

//And also uses EJS as the view engine
app.set('view engine', 'ejs');
app.set('views', './app/views');

module.exports = app;